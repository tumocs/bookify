#BOOKIFY  
This is a small script to split a PDF to 20 page chunks and make small booklet stacks out of them  

##DEPENDENCIES  
pdftk  
java-common-lang  
pdfbook2  
grep  
awk  

##USAGE  
-b pdf
-s size of a single bundle (optional, default 20 pages)
