#!/bin/bash
ITER=0
SIZE=20

while getopts 'b:s:' OPTION; do
    case "$OPTION" in
        s)
        SIZE="$OPTARG"
        ;;
        b)
        BOOK="$OPTARG"
        ;;
        ?)
        echo "Usage: $(basename $0) [-s size] [-b pdf]"
        exit 1
        ;;
    *)
    esac
done

PAGES=$(pdftk "$BOOK" dump_data | grep NumberOfPages | awk '{print $2}')
mkdir booklet

while [ $((ITER*SIZE)) -lt $PAGES ]; do 
    END=$((ITER*SIZE+SIZE))
    if [ $END -gt $PAGES ]; then
        let END=PAGES
    fi
    pdftk "$BOOK" cat $((ITER*SIZE+1))-$END output booklet/p$ITER.pdf
    let ITER=ITER+1
done

for pdf in $( ls booklet ); do 
    pdfbook2 -p a4paper -s -o 2 -t 2 -b 2 -i 80 booklet/$pdf
done

while [ $ITER -gt 0 ]; do 
    rm -v booklet/p$((ITER-1)).pdf 
    let ITER=ITER-1
done
